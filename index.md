[Projects](https://gitlab.com/users/andersh3/contributed)


[OEIS files](https://gitlab.com/OEIS_files)


[Programs](https://gitlab.com/OEIS_files/Programs/blob/master)


[Sequences](https://gitlab.com/OEIS_files/Index/blob/master/Sequences.md)


-------------------------------------------


[Gitlab profile](https://gitlab.com/andersh3)


[OEIS profile](https://oeis.org/wiki/User:Anders_Hellstr%C3%B6m)